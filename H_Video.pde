/*
this class wraps the Movie class, adding some stuff for image processing
 as it is, it has been dialed back to simple video playback,
 but there's still a lot of control methods that should be implemented.
 */
 
import processing.video.*;

class Video {
  PGraphics parent;
  boolean isShowing = false;
  Movie[] movies;
  boolean playing = false;
  float volume = 0.09;
  float alpha = 255;
  boolean fade = false;
  int index = 0;
  float speed = 1;
  boolean forward = false;
  boolean back = false;
  float fadeSpeed = 20;

  Video(PGraphics p, Movie[] moviesT) {
    parent = p;
    movies = moviesT;
    for (int i = 0; i < movies.length; i++) {
      Movie mov = movies[i];
      mov.play();
      mov.pause();
      mov.volume(1);
    }
  }

  void show() {
    try {
      parent.background(0);
      if (forward == true) {
        fade = true;
        if (alpha == 0) {
          movies[index].pause();
          playing = false;
          index++;
          fade = false;
          forward = false;
        }
      }
      if (back == true) {
        fade = true;
        if (alpha == 0) {
          movies[index].pause();
          playing = false;
          index--;
          fade = false;
          back = false;
        }
      }
      if (isShowing) {
        parent.tint(255, alpha);
        parent.image(movies[index], 0, 0, parent.width, parent.height);
      }
    }
    catch (IndexOutOfBoundsException e) {
      index = 0;
    }
    if (fade) {
      alpha -= fadeSpeed;
    } else {
      alpha += fadeSpeed;
    }
    if (alpha > 255) alpha = 255;
    if (alpha < 0) alpha = 0;
  }



  void setSpeed(float newSpeed) { 
    speed = newSpeed;
    movies[index].speed(speed);
    movies[index].play();
  }

  void setVolume(float newVolume) {
    volume = newVolume;
    movies[index].volume(volume);
  }
  
  void setAlpha(float newAlpha) { 
    alpha = newAlpha;
  }

  void setPlaying() {
    playing = !playing;
    if (playing == true) {
      movies[index].play();
    } else {
      movies[index].pause();
    }
  }
  
  void reset() {
    movies[index].jump(0);
    movies[index].play();
    playing = true;
  }



  int getFrame() { //this method just grabs the current frame index;    
    return ceil(movies[index].time() * 30) - 1;
  }
  /*
this method returns the lenght of the movie in frames;
   that is Duration * Frame Rate 
   ex.: 10 seconds * 60 frames/second = 600 frames total;
   */
  int getLength() {
    return int(movies[index].duration() * movies[index].frameRate);
  }

  //void movieEventRunning() {
  //  if (looping == true) {
  //    looper();
  //  }
  //}
}

/*
this method allows the communication between the 
 Movie Thread and the main Processing Thread.
 when the movie thread updates, it calls on this method,
 allowing us to read the frame and manipulate it.
 */
public void movieEvent(Movie m) { 
  m.read();
}
