KinectPV2 kinect = new KinectPV2(this);
Capture cam1;
boolean filesLoaded = false;
boolean debug = true;
boolean mem = false;
boolean keyLocked = false; // this is so i can write on the textfield without triggering the shortcuts
boolean click = false;
PImage uvText;

void settings() {
  //fullScreen(P3D, 2); // when the sketch run, it will be drawn on screen 2 (projector)
  size(800, 600, P3D); // this is just for testing
}
void setup() {
  try {
    startKinect();
    startCameras();
    MIDISetup();
    thread("filesLoading");
    controlScreen = new Control();
    screenSetup(controlScreen);
    mappingSetup();
  } 
  catch (OutOfMemoryError e) {
    exit();
  }
}

void draw() {
  //background(0);
  try {
    background(0);
    controlStats();
    memStats();
    if (filesLoaded) {
      sceneSelection();
    }
    mappingSelection();
  }

  catch (ThreadDeath e) { // this issue happened once and clogged the memory of the computer, so it's better to make sure the Thread yields.
    exit();
    Thread.yield();
  }
  catch (OutOfMemoryError e) {
    exit();
    Thread.yield();
  }
}

void mouseClicked() {
  click = !click;
  //cameras.prevCopy();
}

void keyPressed() {
  keyMappings();
}

public void controlStats() { // this is the debug method, basically allows us to see the mouse and frameRate.
  if (debug) {
    surface.showCursor();
    String txt_fps = "fps: " + (int)frameRate + " || " + "seconds passed: " + frameCount / 60;
    surface.setTitle(txt_fps);
  } else {
    surface.hideCursor();
    surface.setTitle("");
  }
}

public void memStats() {
  if (mem) {
    long maxMemory = Runtime.getRuntime().maxMemory();
    long allocatedMemory = Runtime.getRuntime().totalMemory();
    long freeMemory = Runtime.getRuntime().freeMemory();
    println("Max Memory: " + maxMemory);
    println("Allocated: " + allocatedMemory);
    println("Free: " + freeMemory);
  }
}
