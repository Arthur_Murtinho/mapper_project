/*
the idea for the picture class is that it makes easy
 to project and manipulate images, and image arrays.
 as it is, there is no image processing happening here,
 but it could be added by adding the methods to it.
 */


class Picture {
  PGraphics parent;
  boolean isShowing = false;
  int index = 0;
  ArrayList<PImage> imgs;
  float alpha = 255;
  boolean fadeout = false;

  Picture(PGraphics p, ArrayList<PImage> tImgs) {
    parent = p;
    imgs = tImgs;
    for (PImage img : imgs) {
      img.resize(surface_W, surface_H);
    }
  }

  Picture(PGraphics p, PImage img) {
    parent = p;
    img.resize(surface_W, surface_H);
    imgs = new ArrayList<PImage>();
    imgs.add(img);
  }

  void run() {
    parent.background(0);
    parent.tint(255, alpha);
    if (isShowing == true) {
      try {
        parent.image(imgs.get(index), 0, 0);
      } 
      catch (IndexOutOfBoundsException e) {
        index = 0;
      }
      if (fadeout == true) {
        alpha--;
      } else {
        alpha = 255;
      }
    }
  }
}
