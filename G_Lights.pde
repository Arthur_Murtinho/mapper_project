/*
this is a class for lighting.
 basically the idea is using the projector as 
 a light source for theatre style illumination.
 any processing's that involve graphical programming that
 don't fall under videos, text, or photos should be processed here.
 */

class Light {
  PGraphics parent;
  boolean isShowing = false;
  int index = 0;
  float size;
  float h, s, b, alpha;
  ArrayList<Bulb> bulbs;
  PVector mouseLight = new PVector(0, 0);
  boolean reset = true;
  boolean fade = false;
  float fadeSpeed = 20;

  Light(PGraphics p) {
    parent = p;
    h = 255;
    s = 255;
    b = 255;
    alpha = 255;
    size = 50;
    bulbs = new ArrayList<Bulb>();
  }

  void resetList() {
    if (reset == true) {
      bulbs = new ArrayList<Bulb>();
    }
  }

  void run() {
    parent.colorMode(HSB);
    parent.fill(h, s, b, alpha);
    if (click) {
      mouseLight= surface_4.getTransformedMouse(); // this allows us to place and hold a spotlight
    } 
    if (isShowing) {
      if (index == 0) { // general lights
        reset = true;
        parent.background(h, s, b, alpha);
      }
      if (index == 1) { // movable spotlight
        reset = true;
        parent.background(0);
        parent.ellipse(mouseLight.x, mouseLight.y, size, size);
      }
      if (index == 2) { // trailing fading spot
        reset = false;
        parent.background(h, s, b);
        bulbs.add(new Bulb(parent, mouseLight, true, size));
        for (int i = bulbs.size()-1; i >=0; i--) {
          Bulb b = bulbs.get(i);
          b.run(h);
          if (b.health < 0.0) bulbs.remove(b);
        }
      }
      if (index == 3) { // stroboscopic light
        reset = true;
        float rand = random(1);
        if (rand < 0.25) {
          parent.background(255);
        } else {
          parent.background(0);
        }
      }
      if (index == 4) {// drawing final title
        reset = false;
        parent.background(0, alpha);
        if (click) {
          bulbs.add(new Bulb(parent, mouseLight, false, 25));
        }
        for (int i = bulbs.size()-1; i >=0; i--) {
          Bulb b = bulbs.get(i);
          b.run(255);
        }
      }
      if (index > 4) {
        reset = true;
        parent.background(0);
      }
      resetList();
      if (fade) { // fade function
        alpha -= fadeSpeed;
      } else {
        alpha += fadeSpeed;
      }
      if (alpha > 255) alpha = 255;
      if (alpha < 0) alpha = 0;
    } else {
      reset = true;
      parent.background(0);
    }
  }
}

class Bulb { // this is an ellipse particle for the light effects
  int health = 255;
  PGraphics parent;
  PVector position;
  boolean dying;
  float size;

  Bulb(PGraphics p, PVector pos, boolean d, float s) {
    parent = p;
    position = pos;
    dying = d;
    size = s;
  }

  void run(float c) {
    if (dying) health -= 5;
    parent.noStroke();
    parent.fill(c, health);
    parent.ellipse(position.x, position.y, size, size);
  }
}
