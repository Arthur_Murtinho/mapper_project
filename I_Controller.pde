import themidibus.*;
MidiBus myBus;

public void MIDISetup() {
  MidiBus.list();
  myBus = new MidiBus(this, 0, "Java Sound Synthesizer");
}

void noteOn(int channel, int pitch, int velocity) {
  // Receive a noteOn
  int trigger = pitch-36;
  println();
  println("Note On:");
  println("--------");
  println("Channel:"+channel);
  println("Pitch:"+pitch);
  println("Velocity:"+velocity);
  if (trigger < quotes.dialogueSize) {
    quotes.dialogue.get(trigger).show();
  }
}

void noteOff(int channel, int pitch, int velocity) {
  // Receive a noteOff
  println();
  println("Note Off:");
  println("--------");
  println("Channel:"+channel);
  println("Pitch:"+pitch);
  println("Velocity:"+velocity);
}

void controllerChange(int channel, int number, int value) {
  // Receive a controllerChange
  println();
  println("Controller Change:");
  println("--------");
  println("Channel:"+channel);
  println("Number:"+number);
  println("Value:"+value);
  //int trigger = number-20;
  //if ((trigger < quotes.dialogueSize) || (trigger >= 0)) {
  //  quotes.dialogue.get(trigger).setSpacing(value * 2);
  //}
  if (number == 20) {
    for (Dialogue d : quotes.dialogue) {
      d.setSpacing(value * 2);
    }
  }
}
