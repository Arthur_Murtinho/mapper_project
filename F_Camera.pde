/*
this class handles camera hardware for live video.
 it uses the Capture class from processing video and the Kinect camera, 
 as well as video processing effects;
 */

import KinectPV2.KJoint;
import KinectPV2.*;

class Camera {
  PGraphics parent;
  boolean isShowing = false;
  PVector handPosition;
  PVector previousHandPosition;
  boolean active = false;
  int index = 0;
  PImage prev;
  float threshold = 30;
  boolean metro = false;
  int rate = 10;
  int nFrames = 60; // number of frames in the Frame Buffer, which means this is the video delay size
  ArrayList<PImage> frameBuffer;
  boolean bufferFull = false; // this variable is true once the frame buffer is filled for the first time

  Camera(PGraphics p) {
    parent = p;
    handPosition = new PVector(0, 0);
    //frameBuffer = new PImage[nFrames];
    frameBuffer = new ArrayList<PImage>();
    previousHandPosition = new PVector(0, 0);
    prev = createImage(surface_W, surface_H, RGB);
  }

  void run() {
    parent.background(0);
    if (isShowing) {
      if (index == 0) {
        parent.image(kinect.getColorImage(), 0, 0, surface_W, surface_H);
      }
      if (index == 1) {
        parent.image(kinect.getBodyTrackImage(), 0, 0, surface_W, surface_H);
      }
      if (index == 2) {
        prevCopy();
        movementTracker();
      }
      if (index == 3) {
        drawSkeleton();
      }
      if (index == 4) {
        drawFigure();
      }
      if (index == 5) {
        drawDepthMask();
      }
      if (index == 6) {
        parent.noTint();
        parent.image(cam1, 0, 0, surface_W, surface_H);
        //displayImg(cam1,0xA0ffffff);
      }
      if (index == 7) {
        delayEffect(cam1);
        //RGBDelayEffect(cam1);
      }
      if (index == 8) {
        RGBDelayEffect(cam1);
      }
    } else {
      parent.background(0);
      //cam1.stop();
    }
  }

  void movementTracker() { // this is the movement detection function, which graphs whatever changed from a reference frame.
    PImage kImg;
    kImg = kinect.getColorImage();
    kImg.loadPixels();
    prev.loadPixels();
    for (int x = 0; x < kImg.width; x++) {
      for (int y = 0; y < kImg.height; y++) {
        int loc = x + y * kImg.width;
        color currentColor = kImg.pixels[loc];
        float r1 = red(currentColor);
        float g1 = green(currentColor);
        float b1 = blue(currentColor);
        color prevColor = prev.pixels[loc];
        float r2 = red(prevColor);
        float g2 = green(prevColor);
        float b2 = blue(prevColor);

        float d = distSq(r1, g1, b1, r2, g2, b2);

        if (d < threshold*threshold) { //using black and white, but could be other colors or the previous color of the frame;
          kImg.pixels[loc] = color(255);
          //pixels[loc] = prevColor;
        } else {
          kImg.pixels[loc] = color(0);
          //pixels[loc] = currentColor;
        }
      }
    }
    kImg.updatePixels();
    parent.image(kImg, 0, 0, surface_W, surface_H);
  }

  void prevCopy() { // this method updates the reference frame for the movement tracker, using a metro to update by amount of frames passed.
    if (metro == true) {
      int metro = frameCount % rate;
      if (metro == 0) {
        prev.copy(kinect.getColorImage(), 0, 0, kinect.getColorImage().width, kinect.getColorImage().height, 0, 0, prev.width, prev.height);
        prev.updatePixels();
      }
    }
  }

  void drawFigure() { // the idea here is to invert the bodyTrackImage,, so that we can create a silluete figure;
    parent.colorMode(HSB);
    PImage kImg = kinect.getBodyTrackImage();
    parent.image(kImg, 0, 0, surface_W, surface_H);
    parent.filter(INVERT); // this is very heavy, perhaps creating a frame buffer would help;
  }

  void drawDepthMask() { // this is the mask effect, which highlights the body with a random color;
    parent.image(kinect.getDepthMaskImage(), 0, 0, surface_W, surface_H);
  }

  //DEPRECATED
  void drawFigureOld() { // this is an attempt to invert the body track image so that it maps a white light to the body;
    PImage kImg = kinect.getBodyTrackImage();
    kImg.loadPixels();
    for (int x = 0; x < kImg.width; x++) {
      for (int y = 0; y < kImg.height; y++) {
        int loc = x + y * kImg.width;
        color current = kImg.pixels[loc];
        float red = red(current);
        if (red == 255) {
          kImg.pixels[loc] = color(0);
        } else {
          kImg.pixels[loc] = color(255);
        }
      }
    }
    kImg.updatePixels();
    parent.image(kImg, 0, 0, surface_W, surface_H);
  }

  void pointCloud() {
  }

  void drawSkeleton() { //this is the original skeleton drawing of the body with shapes
    ArrayList <KSkeleton> skeletonArray = kinect.getSkeletonDepthMap();
    parent.image(kinect.getColorImage(), 0, 0, surface_W, surface_H); // for testing the superposition of the skeleton on body;
    for (int i = 0; i < skeletonArray.size(); i++) {
      KSkeleton skeleton = (KSkeleton) skeletonArray.get(i);
      //if the skeleton is being tracked compute the skeleton joints
      if (skeleton.isTracked()) {
        KJoint[] joints = skeleton.getJoints();

        //color col  = skeleton.getIndexColor();
        parent.pushMatrix();
        //parent.fill(col);
        //parent.stroke(col);
        drawBody(joints);
        drawHandState(joints[KinectPV2.JointType_HandRight]);
        drawHandState(joints[KinectPV2.JointType_HandLeft]);
        parent.popMatrix();
      }
    }
  }

  /*
  DELAY EFFECTS AND FRAME BUFFER
   */

  /*
  this took some time to figure out, but here's the deal:
   because we are using a dynamic sized list, instead of a fixed array,
   we don't need to use writing and reading indexes to work it. we can add new frames
   to the top of the list, and always delete the frame at index 0; this makes the whole list
   shift to left, which constrais it to our size, while keeping our "writing" and "reading" indexes
   fixed, at the size index and 0 index;
   there is also a catch when working with the Capture event; we have to check if
   the frame is available, read it, then load its pixels, so that we can copy them. copy() works, not
   sure if get() could work to; we also shouldn't use the direct Capture object in the image() method
   as it will appear to stutter - this is probably because the delayed image is being saved as PImage from
   the loaded pixels, and the direct Capture object will not be loaded on the main thread, only in the Capture
   thread;
   */
  void delayEffect(Capture cam) {
    if (cam.available()) cam.read(); // if there is a frame, read it
    cam.loadPixels(); // we load the current frame's pixels so that we can copy it to a PImage
    PImage img = cam.copy();
    parent.tint(255, 255); // resetting the tint for the synced video
    parent.image(img, 0, 0, surface_W, surface_H); // undelayed image
    frameBuffer.add(img); // last frame added to the list
    if (frameBuffer.size() > nFrames) { // if the list is the size of nFrames, we delete the oldest image, at index 0
      frameBuffer.remove(0);
      bufferFull = true;
    }
    if (bufferFull) {
      parent.tint(255, 127);
      parent.image(frameBuffer.get(0), 0, 0, surface_W, surface_H); // this shows the oldest image
    }
  }

  void RGBDelayEffect(Capture cam) {
    if (cam.available()) cam.read();
    cam.loadPixels();
    PImage img = cam.copy();
    frameBuffer.add(img); 
    parent.tint(255,255);
    displayImg(img, 0xA0ffffff);
    if (frameBuffer.size() > nFrames) {
      frameBuffer.remove(0);
      bufferFull = true;
    }
    if (bufferFull) {
      parent.tint(255,127);
      displayImg(frameBuffer.get(0), 0x80ff0000);
      displayImg(frameBuffer.get(int((nFrames / 3) * 1)), 0x4000ff00);
      displayImg(frameBuffer.get(int((nFrames / 3) * 2)), 0x400000ff);
    }
  }

  void displayImg(PImage img, int mask) {
    img.loadPixels();
    for (int i = 0; i < img.width * img.height; i++) {
      img.pixels[i] &=mask;
    }
    img.updatePixels();
    parent.image(img, 0, 0, surface_W, surface_H);
  }


  void drawBody(KJoint[] joints) { // method for drawing the body
    drawBone(joints, KinectPV2.JointType_Head, KinectPV2.JointType_Neck);
    drawBone(joints, KinectPV2.JointType_Neck, KinectPV2.JointType_SpineShoulder);
    drawBone(joints, KinectPV2.JointType_SpineShoulder, KinectPV2.JointType_SpineMid);
    drawBone(joints, KinectPV2.JointType_SpineMid, KinectPV2.JointType_SpineBase);
    drawBone(joints, KinectPV2.JointType_SpineShoulder, KinectPV2.JointType_ShoulderRight);
    drawBone(joints, KinectPV2.JointType_SpineShoulder, KinectPV2.JointType_ShoulderLeft);
    drawBone(joints, KinectPV2.JointType_SpineBase, KinectPV2.JointType_HipRight);
    drawBone(joints, KinectPV2.JointType_SpineBase, KinectPV2.JointType_HipLeft);

    // Right Arm
    drawBone(joints, KinectPV2.JointType_ShoulderRight, KinectPV2.JointType_ElbowRight);
    drawBone(joints, KinectPV2.JointType_ElbowRight, KinectPV2.JointType_WristRight);
    drawBone(joints, KinectPV2.JointType_WristRight, KinectPV2.JointType_HandRight);
    drawBone(joints, KinectPV2.JointType_HandRight, KinectPV2.JointType_HandTipRight);
    drawBone(joints, KinectPV2.JointType_WristRight, KinectPV2.JointType_ThumbRight);

    // Left Arm
    drawBone(joints, KinectPV2.JointType_ShoulderLeft, KinectPV2.JointType_ElbowLeft);
    drawBone(joints, KinectPV2.JointType_ElbowLeft, KinectPV2.JointType_WristLeft);
    drawBone(joints, KinectPV2.JointType_WristLeft, KinectPV2.JointType_HandLeft);
    drawBone(joints, KinectPV2.JointType_HandLeft, KinectPV2.JointType_HandTipLeft);
    drawBone(joints, KinectPV2.JointType_WristLeft, KinectPV2.JointType_ThumbLeft);

    // Right Leg
    drawBone(joints, KinectPV2.JointType_HipRight, KinectPV2.JointType_KneeRight);
    drawBone(joints, KinectPV2.JointType_KneeRight, KinectPV2.JointType_AnkleRight);
    drawBone(joints, KinectPV2.JointType_AnkleRight, KinectPV2.JointType_FootRight);

    // Left Leg
    drawBone(joints, KinectPV2.JointType_HipLeft, KinectPV2.JointType_KneeLeft);
    drawBone(joints, KinectPV2.JointType_KneeLeft, KinectPV2.JointType_AnkleLeft);
    drawBone(joints, KinectPV2.JointType_AnkleLeft, KinectPV2.JointType_FootLeft);

    //Single joints
    drawJoint(joints, KinectPV2.JointType_HandTipLeft);
    drawJoint(joints, KinectPV2.JointType_HandTipRight);
    drawJoint(joints, KinectPV2.JointType_FootLeft);
    drawJoint(joints, KinectPV2.JointType_FootRight);

    drawJoint(joints, KinectPV2.JointType_ThumbLeft);
    drawJoint(joints, KinectPV2.JointType_ThumbRight);

    drawJoint(joints, KinectPV2.JointType_Head);
  }

  /*
there are in the drawJoints and Bones mappings to translate the points from the depth image (512x424) to the colorImage(1920x1080);
   this is in place so that when projected, the skeleton maps to the body, so that it can be used in projection mapping;
   */

  void drawJoint(KJoint[] joints, int jointType) { //draw a single joint as ellipse
    parent.pushMatrix();
    float jointX = map(joints[jointType].getX(), 0, 512, 0, surface_W);
    float jointY = map(joints[jointType].getY(), 0, 424, 0, surface_H);
    parent.translate(jointX, jointY, joints[jointType].getZ());
    //parent.translate(joints[jointType].getX(), joints[jointType].getY(), joints[jointType].getZ());
    parent.fill(255);
    parent.ellipse(0, 0, 25, 25);
    parent.popMatrix();
  }

  public void drawBone(KJoint[] joints, int jointType1, int jointType2) { //draw a bone from two joints as a stroke
    parent.pushMatrix();
    float jointX1 = map(joints[jointType1].getX(), 0, 512, 0, surface_W);
    float jointY1 = map(joints[jointType1].getY(), 0, 424, 0, surface_H);
    parent.translate(jointX1, jointY1, joints[jointType1].getZ());
    //parent.translate(joints[jointType1].getX(), joints[jointType1].getY(), joints[jointType1].getZ());
    parent.ellipse(0, 0, 25, 25);
    parent.popMatrix();
    parent.stroke(255);
    float jointX2 = map(joints[jointType2].getX(), 0, 512, 0, surface_W);
    float jointY2 = map(joints[jointType2].getY(), 0, 424, 0, surface_H);
    parent.line(jointX1, jointY1, joints[jointType1].getZ(), jointX2, jointY2, joints[jointType2].getZ());
    //parent.line(joints[jointType1].getX(), joints[jointType1].getY(), joints[jointType1].getZ(), joints[jointType2].getX(), joints[jointType2].getY(), joints[jointType2].getZ());
  }

  void drawHandState(KJoint joint) { //draw a ellipse depending on the hand state
    float jointX = map(joint.getX(), 0, 512, 0, surface_W);
    float jointY = map(joint.getY(), 0, 424, 0, surface_H);
    handPosition = new PVector (jointX, jointY);
    //handPosition = new PVector (joint.getX(), joint.getY());
    if (active) {
      parent.noStroke();
      //handState(joint.getState());
      parent.pushMatrix();
      //parent.translate(joint.getX(), joint.getY(), joint.getZ());
      parent.translate(jointX, jointY, joint.getZ());
      parent.fill(255);
      parent.ellipse(0, 0, 70, 70);
      parent.popMatrix();
    }
  }

  /*
Different hand state
   KinectPV2.HandState_Open
   KinectPV2.HandState_Closed
   KinectPV2.HandState_Lasso
   KinectPV2.HandState_NotTracked
   */

  void handState(int handState) { //Depending on the hand state change the color
    switch(handState) {
    case KinectPV2.HandState_Open:
      parent.fill(0, 255, 0);
      active = false;
      break;
    case KinectPV2.HandState_Closed:
      parent.fill(255, 0, 0);
      active = true;
      break;
    case KinectPV2.HandState_Lasso:
      parent.fill(0, 0, 255);
      active = true;
      break;
    case KinectPV2.HandState_NotTracked:
      parent.fill(100, 100, 100);
      active = false;
      break;
    }
  }

  /*in order to properly get the previous and current position in order, we have
   to call the position and assign its value as the previous. when we call the position
   and previous the next frame, it will then always give us the correct positions for the
   tracker. this necessary if you want to work with movement velocity, as you can think of a vector
   between the previous and current position.
   */
  PVector getPreviousHandPosition() {
    return previousHandPosition;
  }

  PVector getPosition() {
    previousHandPosition = handPosition.get();
    return handPosition;
  }
}


public void startKinect() {
  kinect.enableBodyTrackImg(true);
  kinect.enableDepthMaskImg(true);
  kinect.enableDepthImg(true);
  kinect.enableColorImg(true);
  kinect.enableSkeletonDepthMap(true);
  kinect.init();
}

public void startCameras() { //for some reason processing video capture stopped working if it doesn't have this arguments in the creation. it still won't work here, smh;
  //cam1 = new Capture(this, 640, 480, "pipeline:videotestsrc");
  cam1 = new Capture(this, 640, 480, "pipeline:autovideosrc"); //only pipeline that works;
  cam1.start();
  String[] cameraList = Capture.list();
  printArray(cameraList);
}

float distSq(float x1, float y1, float z1, float x2, float y2, float z2) { // we use this instead of the native dist(), because it saves processing power by not having to do roots
  float d = (x2 - x1) * (x2 - x1) + (y2-y1) * (y2 - y1) + (z2 - z1) * (z2 - z1);
  return d;
}

void captureEvent(Capture video) { // movie thread
  video.read();
  //if (cameras.index == 7) {
  //  video.loadPixels();
  //  PImage img = createImage(surface_W, surface_H, RGB);
  //  arrayCopy(video.pixels, img.pixels);
  //  cameras.frameBuffer.add(img);
  //  if (cameras.frameBuffer.size() > cameras.nFrames) {
  //    cameras.bufferFull = true;
  //    cameras.frameBuffer.remove(0);
  //  }
  //}
}
