/*
this is a collection of methods for initializing the keystone projection
and ordering the render process
*/

import deadpixel.keystone.*;

Keystone ks;
CornerPinSurface surface_1;
CornerPinSurface surface_2;
CornerPinSurface surface_3;
CornerPinSurface surface_4;

PGraphics offscreen_1;
PGraphics offscreen_2;
PGraphics offscreen_3;
PGraphics offscreen_4;

final int surface_W = 1920;
final int QuoteSurface_W = 4096;
final int surface_H = 1080;

boolean render_1, render_2, render_3, render_4;


public void mappingSetup() { //surfaces are the mapping screens, and the offscreen is where we render things
  ks = new Keystone(this);
  surface_1 = ks.createCornerPinSurface(surface_W, surface_H, 80); 
  surface_2 = ks.createCornerPinSurface(QuoteSurface_W, surface_H, 80);
  surface_3 = ks.createCornerPinSurface(surface_W, surface_H, 80);
  surface_4 = ks.createCornerPinSurface(surface_W, surface_H, 80);
  offscreen_1 = createGraphics(surface_W, surface_H, P3D);  // high res surface for hd images
  offscreen_2 = createGraphics(QuoteSurface_W, surface_H, P2D);
  offscreen_3 = createGraphics(1920, 1080, P3D); // high res and 3D for the kinect
  offscreen_4 = createGraphics(surface_W, surface_H, P3D);
  render_1 = true;
  render_2 = true;
  render_3 = true;
  render_4 = true;
}

public void mappingSelection() { // the offscreen is rendered in the surface;
  if (render_1) surface_1.render(offscreen_1); 
  if (render_2) surface_2.render(offscreen_2);
  if (render_3) surface_3.render(offscreen_3);
  if (render_4) surface_4.render(offscreen_4);
}
