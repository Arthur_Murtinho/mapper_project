/*
this is the text class, mainly for quotes.
 works like the pictures class, but with text.
 */

class Quote {
  PGraphics parent;
  boolean isShowing = false;
  int quoteScene = 0;
  String citation;
  String[] text;
  int dialogueSize = 6;
  int index = 0;
  float size;
  PFont times;
  ArrayList<Dialogue> dialogue;

  Quote(PGraphics p) {
    parent = p;
    text = loadStrings("data/qt.txt");
    times = createFont("Times New Roman", 70);
    size = 200;
    dialogue = new ArrayList<Dialogue>();
    for (int i = 0; i < dialogueSize; i++) {
      Dialogue d = new Dialogue(parent, i);
      dialogue.add(d);
    }
  }


  void run() {
    parent.background(0);
    try {
      citation = text[index];
    }
    catch (IndexOutOfBoundsException e) {
      index = 0;
    }
    if (isShowing) {
      if (quoteScene == 0) {
        parent.fill(255);
        parent.textFont(times);
        parent.textAlign(CENTER, CENTER);
        parent.textSize(size);
        parent.text(citation, QuoteSurface_W/2, surface_H/2);
      } 
      if (quoteScene == 1) {
        parent.textFont(times);
        for (Dialogue d : dialogue) {
          d.run();
        }
      }
    } else {
      parent.fill(0);
    }
  }
}

class Dialogue {
  int size;
  PGraphics parent;
  XML fullDialogue;
  String currentText = " ";
  String recordedText;
  boolean active = false;
  float linePosition;
  int alignment;
  int lineSpacing = 0;
  int direction = 1;


  Dialogue(PGraphics p, int i) {
    parent = p;
    size = 100;
    fullDialogue = loadXML("data/dialogue.xml");
    XML[] children = fullDialogue.getChildren("line");
    recordedText = children[i].getString("text");
    int tempPosition = children[i].getInt("position");
    switch (tempPosition) {
    case 0:
      linePosition = 0;
      alignment = LEFT;
      direction = 1;
      break;
    case 1:
      linePosition = QuoteSurface_W/2;
      alignment = CENTER;
      direction = 0;
      break;
    case 2:
      linePosition = QuoteSurface_W;
      alignment = RIGHT;
      direction = -1;
      break;
    }
  }

  void run() {
    parent.fill(255);
    parent.textAlign(alignment, CENTER);
    parent.textSize(size);
    if (active) {
      currentText = recordedText;
    } else {
      currentText = " ";
    }
    parent.text(currentText, linePosition + (lineSpacing * direction), surface_H/2);
  }

  void show() {
    active = !active;
  }

  void setSpacing(int newSpacing) {
    lineSpacing = newSpacing;
  }
}
