/*
this is a control window. it creates a second applet,
 and by using the variables declared in this window, we can control
 separately, leaving the first window for sole projection.
 This is also where cp5 is placed, to facilitate the control outside
 of the keyboard.
 */

import controlP5.*;

ControlP5 cp5;
Control controlScreen;
Accordion accordion;

public class Control extends PApplet {

  Control() {
  }

  public void settings() {
    size(800, 600, P2D);
  }

  public void setup() {
    surface.setTitle("Control");
    gui();
  }

  public void draw() {
    background(0);
    sceneDisplay();
  }

  void sceneDisplay() {
    if (filesLoaded) {
      String slide = "";
      String quote = "";
      String cam = "";
      String light = "";

      if (sceneIndex == 0) {
        slide = "Imagem " + str(pictures.index);
      }
      if (sceneIndex == 1) {
        slide = "Video " + str(videos.index);
      }
      if (quotes.citation != null) quote = quotes.citation;
      textSize(20);
      cam = "Cam " + str(cameras.index);
      light = "Light " + str(lights.index); 

      if (pictures.isShowing) fill(255); //using pictures here but it would work with videos, as they have the same button mappings
      else fill(127);
      text(slide, 450, 300);

      if (quotes.isShowing) fill(255);
      else fill(127);
      text(quote, 450, 320);

      if (cameras.isShowing) fill(255);
      else fill(127);
      text(cam, 550, 300);

      if (lights.isShowing) fill(255); 
      else fill(127);
      text(light, 625, 300);

      if (render_1) fill(255);
      else fill(127);
      text("1", 450, 340);
      if (render_2) fill(255);
      else fill(127);
      text("2", 500, 340);
      if (render_3) fill(255);
      else fill(127);
      text("3", 550, 340);
      if (render_4) fill(255);
      else fill(127);
      text("4", 590, 340);

      if (keyLocked)  fill(255);
      else fill(127);
      text("LOCKED", 450, 360);
      if (click) fill(255);
      else fill(127);
      text("WRITING", 450, 380);
    }
  }


  void keyPressed() {
    keyMappings();
  }


  void gui() {
    cp5 = new ControlP5(this);

    cp5.addTextfield("loadMapping")
      .setPosition(450, 20)
      .setSize(100, 25)
      .setFocus(true)
      ;
    cp5.addTextfield("saveMapping")
      .setPosition(450, 60)
      .setSize(100, 25)
      .setFocus(true)
      ;

    cp5.addSlider("Volume")
      .setPosition(450, 120)
      .setSize(125, 15)
      .setRange(0, 1)
      .setValue(0.09)
      ;

    Group g1 = cp5.addGroup("Surfaces")
      .setBackgroundColor(0)
      .setBackgroundHeight(150)
      ;
    cp5.addButton("Images")
      .setPosition(25, 25)
      .setSize(25, 25)
      .moveTo(g1)
      ;
    cp5.addButton("Quotes")
      .setPosition(75, 25)
      .setSize(25, 25)
      .moveTo(g1)
      ;
    cp5.addButton("Kinect")
      .setPosition(25, 75)
      .setSize(25, 25)
      .moveTo(g1)
      ;
    cp5.addButton("Lighting")
      .setPosition(75, 75)
      .setSize(25, 25)
      .moveTo(g1)
      ;

    Group g2 = cp5.addGroup("Slides")
      .setBackgroundColor(0)
      .setBackgroundHeight(150)
      ;
    cp5.addButton("<-")
      .setPosition(25, 25)
      .setSize(25, 25)
      .moveTo(g2)
      ;
    cp5.addButton("->")
      .setPosition(75, 25)
      .setSize(25, 25)
      .moveTo(g2)
      ;
    cp5.addButton("Previous")
      .setPosition(25, 75)
      .setSize(25, 25)
      .moveTo(g2)
      ;
    cp5.addButton("Next")
      .setPosition(75, 75)
      .setSize(25, 25)
      .moveTo(g2)
      ;
    cp5.addButton("Play")
      .setPosition(25, 125)
      .setSize(75, 25)
      .moveTo(g2)
      ;
    cp5.addButton("Show1")
      .setPosition(125, 25)
      .setSize(25, 75)
      .moveTo(g2)
      ;

    Group g3 = cp5.addGroup("Citations")
      .setBackgroundColor(0)
      .setBackgroundHeight(150)
      ;
    cp5.addButton("Back")
      .setPosition(25, 75)
      .setSize(25, 25)
      .moveTo(g3)
      ;
    cp5.addButton("Forward")
      .setPosition(75, 75)
      .setSize(25, 25)
      .moveTo(g3)
      ;
    cp5.addButton("Show2")
      .setPosition(125, 25)
      .setSize(25, 75)
      .moveTo(g3)
      ;

    Group g4 = cp5.addGroup("Graphics")
      .setBackgroundColor(0)
      .setBackgroundHeight(150)
      ;
    cp5.addButton("Silueta")
      .setPosition(25, 25)
      .setSize(25, 25)
      .moveTo(g4)
      ;

    Group g5 = cp5.addGroup("Lights")
      .setBackgroundColor(0)
      .setBackgroundHeight(150);
    ;
    cp5.addSlider("Hue")
      .setPosition(25, 25)
      .setSize(125, 15)
      .setRange(0, 255)
      .setValue(236)
      .moveTo(g5)
      ;

    cp5.addSlider("Saturation")
      .setPosition(25, 75)
      .setSize(125, 15)
      .setRange(0, 255)
      .setValue(44)
      .moveTo(g5)
      ;
    cp5.addSlider("Brightness")
      .setPosition(25, 125)
      .setSize(125, 15)
      .setRange(0, 255)
      .setValue(255)
      .moveTo(g5)
      ;
    cp5.addSlider("Alpha")
      .setPosition(225, 25)
      .setSize(15, 125)
      .setRange(0, 255)
      .setValue(255)
      .moveTo(g5)
      ;
    cp5.addSlider("Size") 
      .setPosition(25, 175)
      .setSize(125, 15)
      .setRange(15, 250)
      .setValue(50)
      .moveTo(g5)
      ;

    accordion = cp5.addAccordion("settings")
      .setPosition(10, 10)
      .setWidth(400)
      .setHeight(600)
      .addItem(g1)
      .addItem(g2)
      .addItem(g3)
      .addItem(g4)
      .addItem(g5)
      ;
    //accordion.open(0, 1);
    //accordion.setCollapseMode(Accordion.MULTI);
    accordion.setCollapseMode(Accordion.SINGLE);
  }

  public void controlEvent(ControlEvent event) {
    //println(event.getController().getName());
    if (filesLoaded) {
      if (event.getController().getName() == "Images") {
        render_1 = !render_1;
      }
      if (event.getController().getName() == "Quotes") {
        render_2 = !render_2;
      }
      if (event.getController().getName() == "Kinect") {
        render_3 = !render_3;
      }
      if (event.getController().getName() == "Lighting") {
        render_4 = !render_4;
      }


      if (event.getController().getName() == "<-") {
        sceneIndex--;
      }
      if (event.getController().getName() == "->") {
        sceneIndex++;
      }
      if (event.getController().getName() == "Previous") {
        pictures.index--;
      }
      if (event.getController().getName() == "Next") {
        pictures.index++;
      }
      if (event.getController().getName() == "Play") {
        videos.setPlaying();
      }

      if (event.getController().getName() == "Back") {
        quotes.index--;
      }
      if (event.getController().getName() == "Forward") {
        quotes.index++;
      }

      if (event.getController().getName() == "Silueta") {
        //skeleton.isShowing = !skeleton.isShowing;
      }

      if (event.getController().getName() == "Show1") {
        videos.isShowing = !videos.isShowing;
        pictures.isShowing = !pictures.isShowing;
      }
      if (event.getController().getName() == "Show2") {
        quotes.isShowing = !quotes.isShowing;
      }

      if (event.getController().getName() == "Hue") {
        lights.h = event.getController().getValue();
      }
      if (event.getController().getName() == "Saturation") {
        lights.s = event.getController().getValue();
      }
      if (event.getController().getName() == "Brightness") {
        lights.b = event.getController().getValue();
      }
      if (event.getController().getName() == "Alpha") {
        lights.alpha = event.getController().getValue();
      }
      if (event.getController().getName() == "Size") {
        lights.size = event.getController().getValue();
      }

      if (event.getController().getName() == "loadMapping") { // this event loads the input text for mapping
        String fileName = "data/" + event.getStringValue() + ".xml";
        try {
          ks.load(fileName);
        } 
        catch (ArrayIndexOutOfBoundsException e) {
          e.getCause();
          e.printStackTrace();
        }
      }
      if (event.getController().getName() == "saveMapping") { // and this one saves to the input
        String fileName = "data/" + event.getStringValue() + ".xml";
        ks.save(fileName);
      }
      if (event.getController().getName() == "Volume") {
        videos.setVolume(event.getController().getValue());
      }
    }
  }

  public void keyMappings() { //key Mappings are duplicated because the method must be inside the other PApplet to run correctly;
    switch(key) {
    case '\'':
      keyLocked = !keyLocked;
      break;
    }
    if (!keyLocked) {
      switch(key) {
      case TAB:
        debug = !debug;
        break;
      case ALT:
        mem = !mem;
        break;
      case '1':
        render_1 = !render_1;
        break;
      case '2':
        render_2 = !render_2;
        break;
      case '3':
        render_3 = !render_3;
        break;
      case '4':
        render_4 = !render_4;
        break;

      case 'c': // enter/leave calibration mode, where surfaces can be warped and moved
        ks.toggleCalibration();
        break;
      case 's':
        // save keystone file, for loading later.
        ks.save("data/keystone.xml");
        break;
      case 'l': // load default keystone file
        try {
          ks.load();
        } 
        catch (ArrayIndexOutOfBoundsException e) {
          e.getCause();
          e.printStackTrace();
        }
        catch (NullPointerException e) {
          e.printStackTrace();
          println("No standart file to load!");
        }
        break;

      case '.':
        quotes.index++;
        break;
      case ',':
        quotes.index--;
        break;
      case '/':
        quotes.isShowing = !quotes.isShowing;
        break;
      case '9':
        quotes.quoteScene--;
        break;
      case '0':
        quotes.quoteScene++;
        break;

      case 'n':
        pictures.index++;
        videos.forward = true;
        break;
      case 'b':
        pictures.index--;
        videos.back = true;
        break;
      case 'm':
        videos.isShowing = !videos.isShowing;
        pictures.isShowing = !pictures.isShowing;
        break;
      case 'v':
        pictures.fadeout = !pictures.fadeout;
        videos.fade = !videos.fade;
        break;

      case '-':
        sceneIndex--;
        println(sceneIndex);
        break;
      case '=':
        sceneIndex++;
        println(sceneIndex);
        break;

      case 'p':
        videos.setPlaying();
        break;
      case 'o': 
        videos.reset();
        break;

      case 'k':
        cameras.isShowing = !cameras.isShowing;
        break;
      case '´':
        cameras.index++;
        //println("Camera: " + cameras.index);
        break;
      case '~':
        cameras.index--;
        //println("Camera: " + cameras.index);
        break;

      case '[':
        lights.index++;
        //println("Lighting: " + lights.index);
        break;
      case ']':
        lights.index--;
        //println("Lighting: " + lights.index);
        break;
      case ';':
        lights.isShowing = !lights.isShowing;
        break;
      }
    }
  }
}

public static void screenSetup(PApplet sa) {
  String[] args = {"Control_Screen"};
  PApplet.runSketch(args, sa);
}
