/*
this is a collection of functions to organize
 the selection of scenes and the general setup of all
 objects in draw.
 */

Video videos;
Picture pictures;
Quote quotes;
Camera cameras;
Light lights;
int sceneIndex = 0;

/*
Pictures and Movie are placed in offscreen 1
 quotes on offscreen 2
 skeleton on offscreen 3
 */


/*
so about Video, processing needs the EXACT path to find the movie file.
 it will not work, even if referenced by "data/file"
 */

/*
this function is called by using thread() in the setup function,
 which means every loading that happens here happens asynchronously
 this is useful, because it allows us to load the mapping position and other general
 things without having to wait for every photo to load.
 the filesLoaded boolean in the main folder is what controls the flow of this, and is set true
 by the end of the function;
 */

public void filesLoading() { // this method loads all files and inits the classes
  uvText = loadImage("data/default.png");
  println("uv map loaded");
  pictures = new Picture(offscreen_1, new ArrayList<PImage>());
  Movie[] movies = new Movie[1];
  movies[0] = new Movie(this, "C:/Users/Arthur/Desktop/MAPPA/data/test.mp4");
  videos = new Video(offscreen_1, movies);
  quotes = new Quote(offscreen_2);
  cameras = new Camera(offscreen_3);
  printArray(Capture.list());
  //cam1 = new Capture(this, "pipeline:autovideosrc");
  //cam1.start();
  lights = new Light(offscreen_4);
  filesLoaded = true;
  println("all files loaded");
}

public void sceneSelection() { // this is where we pick and render every offscreen
  if (ks.isCalibrating()) {
    offscreen_1.beginDraw();
    offscreen_1.image(uvText, 0, 0, surface_W, surface_H);
    offscreen_1.endDraw();
    offscreen_2.beginDraw();
    offscreen_2.image(uvText, 0, 0, QuoteSurface_W, surface_H);
    offscreen_2.endDraw();
    offscreen_3.beginDraw();
    offscreen_3.image(uvText, 0, 0, surface_W, surface_H);
    offscreen_3.endDraw();
    offscreen_4.beginDraw();
    offscreen_4.image(uvText, 0, 0, surface_W, surface_H);
    offscreen_4.endDraw();
  } else {
    if (render_1) {
      offscreen_1.beginDraw();
      offscreen_1.background(0);
      if (sceneIndex == 0) pictures.run();
      if (sceneIndex == 1) {
        videos.show();
        //if (videos.mov.time() == videos.mov.duration()) {
        //  videos.mov.jump(0);
        //  videos.mov.pause();
        //}
      }
      offscreen_1.endDraw();
    }

    if (render_2) {
      offscreen_2.beginDraw();
      quotes.run();
      offscreen_2.endDraw();
    }

    if (render_3) {
      offscreen_3.beginDraw();
      cameras.run();
      offscreen_3.endDraw();
    }

    if (render_4) {
      offscreen_4.beginDraw();
      lights.run();
      offscreen_4.endDraw();
    }
  }
}


public void keyMappings() {
  switch(key) {
  case '\'':
    keyLocked = !keyLocked;
    break;
  }
  if (!keyLocked) {
    switch(key) {
    case TAB:
      debug = !debug;
      break;
    case ALT:
      mem = !mem;
      break;
    case '1':
      render_1 = !render_1;
      break;
    case '2':
      render_2 = !render_2;
      break;
    case '3':
      render_3 = !render_3;
      break;
    case '4':
      render_4 = !render_4;
      break;

    case 'c': // enter/leave calibration mode, where surfaces can be warped and moved
      ks.toggleCalibration();
      break;
    case 's':
      // save keystone file, for loading later.
      ks.save("data/keystone.xml");
      break;
    case 'l': // load default keystone file
      try {
        ks.load();
      } 
      catch (ArrayIndexOutOfBoundsException e) {
        e.getCause();
        e.printStackTrace();
      }
      catch (NullPointerException e) {
        e.printStackTrace();
        println("No standart file to load!");
      }
      break;

    case '.':
      quotes.index++;
      break;
    case ',':
      quotes.index--;
      break;
    case '/':
      quotes.isShowing = !quotes.isShowing;
      break;
    case '9':
      quotes.quoteScene--;
      break;
    case '0':
      quotes.quoteScene++;
      break;

    case 'n':
      pictures.index++;
      videos.forward = true;
      //videos.index++;
      break;
    case 'b':
      pictures.index--;
      videos.back = true;
      break;
    case 'm':
      videos.isShowing = !videos.isShowing;
      pictures.isShowing = !pictures.isShowing;
      break;
    case 'v':
      pictures.fadeout = !pictures.fadeout;
      videos.fade = !videos.fade;
      break;

    case '-':
      sceneIndex--;
      println(sceneIndex);
      break;
    case '=':
      sceneIndex++;
      println(sceneIndex);
      break;

    case 'p':
      videos.setPlaying();
      break;
    case 'o': 
      videos.reset();
      break;

    case 'k':
      cameras.isShowing = !cameras.isShowing;
      break;
    case '´':
      cameras.index++;
      //println("Camera: " + cameras.index);
      break;
    case '~':
      cameras.index--;
      //println("Camera: " + cameras.index);
      break;
    case 't':
      cameras.metro = !cameras.metro;
      break;

    case '[':
      lights.index++;
      //println("Lighting: " + lights.index);
      break;
    case ']':
      lights.index--;
      //println("Lighting: " + lights.index);
      break;
    case ';':
      lights.isShowing = !lights.isShowing;
      break;
    }
  }
}
